package com.hendisantika.springboot.jwt.integration.repository;

import com.hendisantika.springboot.jwt.integration.domain.Role;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-jwt
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/21/17
 * Time: 5:45 AM
 * To change this template use File | Settings | File Templates.
 */
public interface RoleRepository extends CrudRepository<Role, Long> {
}
