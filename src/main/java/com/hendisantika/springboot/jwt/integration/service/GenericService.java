package com.hendisantika.springboot.jwt.integration.service;

import com.hendisantika.springboot.jwt.integration.domain.RandomCity;
import com.hendisantika.springboot.jwt.integration.domain.User;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-jwt
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/21/17
 * Time: 5:48 AM
 * To change this template use File | Settings | File Templates.
 */
public interface GenericService {
    User findByUsername(String username);

    List<User> findAllUsers();

    List<RandomCity> findAllRandomCities();
}
