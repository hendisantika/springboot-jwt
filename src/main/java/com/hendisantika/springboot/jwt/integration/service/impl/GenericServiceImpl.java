package com.hendisantika.springboot.jwt.integration.service.impl;

import com.hendisantika.springboot.jwt.integration.domain.RandomCity;
import com.hendisantika.springboot.jwt.integration.domain.User;
import com.hendisantika.springboot.jwt.integration.repository.RandomCityRepository;
import com.hendisantika.springboot.jwt.integration.repository.UserRepository;
import com.hendisantika.springboot.jwt.integration.service.GenericService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-jwt
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/21/17
 * Time: 5:49 AM
 * To change this template use File | Settings | File Templates.
 */
@Service
public class GenericServiceImpl implements GenericService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RandomCityRepository randomCityRepository;

    @Override
    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public List<User> findAllUsers() {
        return (List<User>)userRepository.findAll();
    }

    @Override
    public List<RandomCity> findAllRandomCities() {
        return (List<RandomCity>)randomCityRepository.findAll();
    }
}
